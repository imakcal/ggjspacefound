﻿using System;
using System.Collections;
using UnityEngine;

namespace Game.Scripts.Behaviours
{
    public class ShipBehavior : MonoBehaviour
    {
        [SerializeField] protected ParticleSystem explosionParticle;
        [SerializeField] protected Rigidbody[] shipParts;
        [SerializeField] protected GameObject laserPrefab;
        [SerializeField] protected float fireInterval;
        [SerializeField] protected float shipSpeed;
        [SerializeField] protected int health;
        [SerializeField] protected int maxHealth;
        [SerializeField] protected AudioClip explosionSound;
        [SerializeField] protected AudioClip fireSound;
        [SerializeField] protected AudioSource soundPlayer;
        [SerializeField] protected Transform[] laserShootPoints;
        private bool _lasersReloaded = true;
        
        public void HitTaken(int damageAmount)
        {
            ChangeHealth(-damageAmount);
            Debug.Log("Hit: " + gameObject.name);
        }
        
        protected void Fire()
        {
            if (!_lasersReloaded) return;
            foreach (var laserShootPoint in laserShootPoints)
            {
                soundPlayer.PlayOneShot(fireSound);
                Instantiate(laserPrefab, laserShootPoint.transform.position, transform.rotation);
            }

            StartCoroutine(Reload());
        }

        protected virtual void ChangeHealth(int changeInHealth)
        {
            health = Mathf.Clamp(health + changeInHealth, 0, maxHealth);

            if (health == 0)
            {
                DestroyShip();
            }
        }

        protected virtual void DestroyShip()
        {
            Instantiate(explosionParticle, transform.position, Quaternion.identity);
            soundPlayer.PlayOneShot(explosionSound);
        }

        private IEnumerator Reload()
        {
            _lasersReloaded = false;
            yield return new WaitForSeconds(fireInterval);
            _lasersReloaded = true;
        }
    }
}