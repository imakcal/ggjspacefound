﻿using System;
using System.Collections.Generic;
using Game.Scripts.Controllers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Scripts.Behaviours
{
    public class EnemyShipBehaviour : ShipBehavior
    {
        public static event Action<int> UpdateScore;
        public static event Action<string> PlayerDetected;
        public static event Action<string> Destroyed;
        public static event Action<PlayerShipBehaviour> DetectedPlayer;

        [SerializeField] private float firingRange;
        [SerializeField] private int scorePoint;

        private List<Transform> enemyRandomTargets;
        private bool isMovingToRandom;
        private Vector3 _targetDirection;
        private Transform _targetPoint;
        public bool isABoss;

        private PlayerShipBehaviour _playerShip;
        private Vector3 _selfPosition => transform.position;

        protected void OnShipDestroyed(string message)
        {
            Destroyed?.Invoke(message);
        }

        private void Update()
        {
            CheckPlayer();
            if (!_playerShip) return;

            if (isMovingToRandom)
            {
                if (ReachedRandomDestination())
                {
                    isMovingToRandom = false;
                }
                else
                {
                    RotateToEnemy();
                    MoveForward();
                }
            }
            else
            {
                if (CheckForRandomChance() && !isABoss)
                {
                    isMovingToRandom = true;
                    _targetPoint = enemyRandomTargets[Random.Range(0, enemyRandomTargets.Count - 1)];
                    _targetDirection = (_targetPoint.position - transform.position).normalized;
                }
                else
                {
                    _targetDirection = (_playerShip.transform.position - transform.position).normalized;

                    RotateToEnemy();
                    MoveForward();
                    FireCheck();
                }
            }
        }

        private void CheckPlayer()
        {
            if (!(Vector3.Distance(transform.position, GameController.instance.playerShip.transform.position) <
                  200f)) return;
            _playerShip = GameController.instance.playerShip;
            DetectedPlayer?.Invoke(_playerShip);
            PlayerDetected?.Invoke("ENEMY APPROACHING!");
        }

        private bool ReachedRandomDestination()
        {
            return Vector3.Distance(_targetPoint.position, _selfPosition) < 10f;
        }

        private bool CheckForRandomChance()
        {
            return Random.Range(0f, 1f) < 0.001;
        }

        private void MoveForward()
        {
            transform.position += transform.forward * (Time.deltaTime * shipSpeed);
        }

        private void RotateToEnemy()
        {
            if (!isABoss)
            {
                transform.LookAt(_playerShip.transform.position);
            }
        }

        private void FireCheck()
        {
            if (Vector3.Distance(_playerShip.transform.position, _selfPosition) < firingRange)
            {
                Fire();
            }
        }

        public void GetRandomPoints(List<Transform> randomPoints)
        {
            enemyRandomTargets = randomPoints;
        }

        protected override void DestroyShip()
        {
            Instantiate(explosionParticle);
            OnShipDestroyed("Enemy destroyed!");
            UpdateScore?.Invoke(scorePoint);
            Destroy(gameObject);
        }
    }
}