﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Game.Scripts.Behaviours
{
    public class PortalBehaviour : MonoBehaviour
    {
        public static event Action PortalEnter;

        [SerializeField] private Transform cicle;

        private void Awake()
        {
            if (cicle)
            {
                cicle.DORotate(new Vector3(0f, 0f, 360f), 5f, RotateMode.FastBeyond360 ).SetLoops(-1);
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer != LayerMask.NameToLayer("Player")) return;
            var isAShip = other.gameObject.GetComponent<ShipBehavior>();
            if (isAShip)
            {
                PortalEnter?.Invoke();
            }
        }
    }
}