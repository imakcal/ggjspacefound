﻿using System;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Scripts.Behaviours
{
    public class DebrisBehaviour : MonoBehaviour
    {
        public static event Action<int> UpdateScore;

        [SerializeField] private float RotationSpeed;
        [SerializeField] private float DriftingSpeed;
        [SerializeField] private int scorePoint;
        
        private int _target;
        private Vector3 rotationAxis;
        private Vector3 driftingDirection;

        void Start()
        {
            driftingDirection = new Vector3(Random.Range(0f, 100f), Random.Range(0f, 100f), Random.Range(0f, 100f))
                .normalized;

            transform.DOMove(transform.position + driftingDirection * 10f, 10).SetLoops(-1, LoopType.Yoyo);
            transform.DORotate(new Vector3(Random.Range(0, 2) * 360f, Random.Range(0, 2) * 360f, Random.Range(0, 2) * 360f), 10f ).SetLoops(-1);
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.layer != LayerMask.NameToLayer("Player")) return;

            var ship = other.gameObject.GetComponent<ShipBehavior>();

            if (!ship) return;
            ship.HitTaken(10);
            DestroyDebris();
        }

        public void DestroyDebris()
        {
            UpdateScore?.Invoke(scorePoint);
            Destroy(gameObject);
        }
    }
}