﻿using System;
using UnityEngine;

namespace Game.Scripts.Behaviours
{
    public class HealthPackBehavior : MonoBehaviour
    {
        [SerializeField] private int healthPoint;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer != LayerMask.NameToLayer("Player")) return;
            other.gameObject.GetComponent<PlayerShipBehaviour>().GainHealth(healthPoint);
            Destroy(gameObject);
        }
    }
}
