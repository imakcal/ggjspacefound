﻿using System;
using System.Collections;
using Cinemachine;
using UnityEngine;

namespace Game.Scripts.Behaviours
{
    public class PlayerShipBehaviour : ShipBehavior
    {
        public static event Action<int, int> HealthChanged;
        public static event Action<string> Destroyed;

        [SerializeField] private float pitchSpeed; // Rotates on self x 
        [SerializeField] private float yawSpeed; // Rotates on self y
        [SerializeField] private float rollSpeed; // Rotates on self z
        [SerializeField] private MeshFilter selfMeshFilter;
        [SerializeField] private CinemachineVirtualCamera cockpitCam;
        [SerializeField] private CinemachineVirtualCamera cockpitOuterCam;
        [SerializeField] private GameObject hyperdrive;
        [SerializeField] private GameObject hatch;
        private bool gameActive;
        
        protected void OnShipDestroyed(string message)
        {
            Destroyed?.Invoke(message);
        }
        
        public void OnLevelStarted()
        {
            gameActive = true;
            hyperdrive.SetActive(false);
            EnableOuterCockpitView(false);
            ToggleHatch(false);
        }
        
        public void OnLevelEnded()
        {
            gameActive = false;
            hyperdrive.SetActive(true);
            EnableOuterCockpitView(true);
            ToggleHatch(true);
        }

        public void GainHealth(int gainAmount)
        {
            ChangeHealth(gainAmount);
        }

        private void RemoveAllParts()
        {
            var transform1 = transform;
            var selfPosition = transform1.position;
            var selfParent = transform1.parent;

            foreach (var shipPart in shipParts)
            {
                shipPart.isKinematic = false;
                shipPart.transform.parent = selfParent;
                shipPart.AddForce((shipPart.transform.position - selfPosition) * 400);
            }

            Destroy(gameObject);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                ChangeHealth(-50);
            }
            
            if (!gameActive) return;
            if (Input.anyKey)
            {
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    Pitch(1);
                }

                if (Input.GetKey(KeyCode.DownArrow))
                {
                    Pitch(-1);
                }

                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    Yaw(-1);
                }

                if (Input.GetKey(KeyCode.RightArrow))
                {
                    Yaw(1);
                }

                if (Input.GetKey(KeyCode.Z))
                {
                    Roll(-1);
                }

                if (Input.GetKey(KeyCode.X))
                {
                    Roll(1);
                }

                if (Input.GetKey(KeyCode.Space))
                {
                    Fire();
                }

                if (Input.GetKeyDown(KeyCode.C))
                {
                    EnableCockpitView();
                }
            }
            else
            {
                // Vector3 eulerRotation = transform.rotation.eulerAngles;
                // transform.rotation = Quaternion.Euler(eulerRotation.x, eulerRotation.y, Mathf.Lerp(0, eulerRotation.z, 0.5f));
                
                
                // var selfRotation = transform.localRotation;
                // transform.rotation = Quaternion.Slerp(selfRotation,
                //     new Quaternion(selfRotation.x, selfRotation.y, 0f, selfRotation.w), 0.04f);
            }

            transform.position += transform.forward * (Time.deltaTime * shipSpeed);
        }

        private void Pitch(int sign)
        {
            transform.Rotate(pitchSpeed * sign * Time.deltaTime, 0f, 0f);
        }

        private void ToggleHatch(bool isOpen)
        {
            if (isOpen)
            {
                // TODO: OPEN HATCH
            }
            else
            {
                // TODO: CLOSE HATCH
            }
        }

        private void Yaw(int sign)
        {
            transform.Rotate(0f, yawSpeed * sign * Time.deltaTime, 0f);
        }

        private void Roll(int sign)
        {
            transform.Rotate(0f, 0f, rollSpeed * sign * Time.deltaTime);
        }

        private void EnableCockpitView()
        {
            cockpitCam.gameObject.SetActive(!cockpitCam.gameObject.activeSelf);
        }
        
        private void EnableOuterCockpitView(bool isActive)
        {
            cockpitOuterCam.gameObject.SetActive(isActive);
        }

        protected override void DestroyShip()
        {
            Instantiate(explosionParticle);
            selfMeshFilter.mesh = null;
            OnShipDestroyed("Mission Failed, we'll get em next time");
            RemoveAllParts();
        }

        protected override void ChangeHealth(int changeInHealth)
        {
            base.ChangeHealth(changeInHealth);
            HealthChanged?.Invoke(health, maxHealth);
        }
    }
}