﻿using System;
using UnityEngine;

namespace Game.Scripts.Behaviours
{
    public class TurretBehaviour : MonoBehaviour
    {
        private PlayerShipBehaviour _playerShip;

        private void Awake()
        {
            EnemyShipBehaviour.DetectedPlayer += OnPlayerDetected;
        }

        private void OnPlayerDetected(PlayerShipBehaviour pPlayerShip)
        {
            _playerShip = pPlayerShip;
        }

        private void OnDestroy()
        {
            EnemyShipBehaviour.DetectedPlayer -= OnPlayerDetected;
        }

        private void Update()
        {
            if (_playerShip)
            {
                transform.LookAt(_playerShip.transform.position);
            }
        }
    }
}