﻿using DG.Tweening;
using UnityEngine;

namespace Game.Scripts.Behaviours
{
    public class PlanetBehaviour : MonoBehaviour
    {
        void Start()
        {
            transform.DORotate(new Vector3(0, 360f, 0),2.5f , RotateMode.FastBeyond360).SetLoops(-1);
        }
    }
}
