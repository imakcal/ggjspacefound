﻿using System;
using UnityEngine;

namespace Game.Scripts.Behaviours
{
    public class LaserBehaviour : MonoBehaviour
    {
        [SerializeField] private int damagePoint;
        [SerializeField] private ParticleSystem explosionParticle;
        [SerializeField] protected AudioClip explosionSound;
        [SerializeField] protected AudioSource soundPlayer;

        private void Start()
        {
            Destroy(gameObject, 1f);
        }

        void Update()
        {
            transform.position += transform.forward * (Time.deltaTime * 500f);
        }

        private void OnTriggerEnter(Collider other)
        {
            var shipHit = other.gameObject.GetComponent<ShipBehavior>();
            var debrisHit = other.gameObject.GetComponent<DebrisBehaviour>();
            if (!shipHit && !debrisHit) return;
            if (shipHit)
            {
                shipHit.HitTaken(damagePoint);
            }
            else if (debrisHit)
            {
                debrisHit.DestroyDebris();
            }

            soundPlayer.PlayOneShot(explosionSound);
            Instantiate(explosionParticle, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}