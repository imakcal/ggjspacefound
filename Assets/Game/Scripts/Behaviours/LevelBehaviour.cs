﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Scripts.Behaviours
{
    public enum LevelType
    {
        Asteroids,
        Enemies
    }
    
    public class LevelBehaviour : MonoBehaviour
    {
        public static event Action<string> PlayerLeavingArea;
        public static event Action<string> PortalSpawned;
        public event Action LevelReady;
        public event Action LevelCompleted;
        
        [SerializeField] private DebrisBehaviour[] _debrices;
        [SerializeField] private EnemyShipBehaviour[] _easyEnemies;
        [SerializeField] private EnemyShipBehaviour[] _mediumEnemies;
        [SerializeField] private EnemyShipBehaviour[] _bossEnemies;
        [SerializeField] private LevelType _levelType;
        [SerializeField] private bool _bossLevel;
        public Transform SpawnPoint;
        public int scoreRequirement;
        [SerializeField] private  SphereCollider gameAreaTriggerer;
        [SerializeField] private  PortalBehaviour portal;
        [SerializeField] private  HealthPackBehavior healthPackPrefab;
        private Vector3 _portalPoint;
        private List<Transform> _randomDestinations = new List<Transform>();
        private float areaRadius;
        private Vector3 areaCenter;
        private bool isPortalOpen;
        
        public void Initialize()
        {
            areaRadius = gameAreaTriggerer.radius;
            areaCenter = transform.position;
            
            switch (_levelType)
            {
                case LevelType.Asteroids:
                    ConstructAsteroidField();
                    break;
                
                case LevelType.Enemies:
                    SpawnEnemies(_bossLevel);
                    break;
            }
        }
        
        private void ConstructAsteroidField()
        {
            StartCoroutine(AsteroidFieldConstructor());
        }

        private IEnumerator AsteroidFieldConstructor()
        {
            var debrisCount = _debrices.Length;
            
            for (int i = 0; i < 500; i++)
            {
                 var newAsteroid = Instantiate(_debrices[Random.Range(0, debrisCount - 1)], GetRandomPos(), Quaternion.identity);
                 newAsteroid.transform.parent = transform;
                 yield return null;
            }
            
            for (int i = 0; i < 100; i++)
            {
                var healthPack = Instantiate(healthPackPrefab, GetRandomPos(), Quaternion.identity);
                healthPack.transform.parent = transform;
                yield return null;
            }
            
            OnLevelLoaded();
        }
        
        private void SpawnEnemies(bool isBossLevel)
        {
            StartCoroutine(EnemySpawner(isBossLevel));
        }

        private Vector3 GetRandomPos()
        {
            return (areaCenter + new Vector3(Random.Range(-areaRadius, areaRadius), Random.Range(-areaRadius, areaRadius), Random.Range(-areaRadius, areaRadius))/2);
        }

        private IEnumerator EnemySpawner(bool isBossLevel)
        {
            if (!isBossLevel)
            {
                for (int i = 0; i < 10; i++)
                {
                    var newPoint = Instantiate(new GameObject(), transform);
                    newPoint.transform.position = GetRandomPos();
                    _randomDestinations.Add(newPoint.transform);
                    yield return null;
                }
                
                for (int i = 0; i < 100; i++)
                {
                    var healthPack = Instantiate(healthPackPrefab, GetRandomPos(), Quaternion.identity);
                    healthPack.transform.parent = transform;
                    yield return null;
                }
                
                var easyEnemyCount = Random.Range(3,7);
                var mediumEnemyCount = Random.Range(2,4);
                var easyEnemyListLength = _easyEnemies.Length;
                var mediumEnemyListLength = _mediumEnemies.Length;
            
                for (int i = 0; i < easyEnemyCount; i++)
                {
                     var newEasyEnemy = Instantiate(_easyEnemies[Random.Range(0, easyEnemyListLength)], GetRandomPos(), Quaternion.identity);
                     newEasyEnemy.GetRandomPoints(_randomDestinations);
                     newEasyEnemy.transform.parent = transform;
                     yield return null;
                }
            
                for (int i = 0; i < mediumEnemyCount; i++)
                {
                    var newMediumEnemy = Instantiate(_mediumEnemies[Random.Range(0, mediumEnemyListLength)], GetRandomPos(), Quaternion.identity);
                    newMediumEnemy.GetRandomPoints(_randomDestinations);
                    newMediumEnemy.transform.parent = transform;
                    yield return null;
                }
            }
            else
            {
                var hardEnemyListLength = _bossEnemies.Length;
                var hardEnemy = Instantiate(_bossEnemies[Random.Range(0, hardEnemyListLength)], GetRandomPos(), Quaternion.identity);
                hardEnemy.isABoss = true;
                hardEnemy.transform.parent = transform;
                yield return null;
            }

            _portalPoint = GetRandomPos();
            OnLevelLoaded();
        }
        
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer != LayerMask.NameToLayer("Player")) return;
            var ship = other.gameObject.GetComponent<ShipBehavior>();

            if (!ship) return;
            other.transform.LookAt(transform.position);
            PlayerLeavingArea?.Invoke("You are leaving the mission area!");

        }

        public void OnMissionsCompleted()
        {
            SpawnPortal();
        }

        private void SpawnPortal()
        {
            if (isPortalOpen) return;
            isPortalOpen = true;
            var newPortal = Instantiate(portal, _portalPoint, Quaternion.identity);
            newPortal.transform.parent = transform;
            PortalBehaviour.PortalEnter += OnLevelCompleted;
            PortalSpawned?.Invoke("PORTAL SPAWNED. ENTER IT FOR THE NEXT LEVEL!");
        }

        private void OnLevelCompleted()
        {
            PortalBehaviour.PortalEnter -= OnLevelCompleted;
            LevelCompleted?.Invoke();
        }
        
        private void OnLevelLoaded()
        {
            LevelReady?.Invoke();
        }

        public void DisposeLevel()
        {
            Destroy(gameObject);
        }
    }
}
