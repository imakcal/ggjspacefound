﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game.Scripts.Behaviours;
using UnityEngine;

namespace Game.Scripts.Controllers
{
    public class LevelController : MonoBehaviour
    {
        public event Action NextLevelReady;
        public event Action LevelCompleted;
        public static event Action<int> LevelScoreDecided;
        public static event Action<string> HyperDriveMessage;
        public static event Action GameOver;
        
        [SerializeField] private List<LevelBehaviour> _levelsList;

        public LevelBehaviour CurrentLevel;
        private bool waitInHyperdrive;

        public void OnGameStarted()
        {
            StartFirstLevel();
        }

        private void StartFirstLevel()
        {
            OpenLevel();
        }

        private void OpenLevel()
        {
            var levelToOpen = _levelsList[0];
            _levelsList.RemoveAt(0);
            CurrentLevel = Instantiate(levelToOpen, transform);
            CurrentLevel.transform.position = Vector3.zero;
            CurrentLevel.transform.rotation = Quaternion.identity;
            CurrentLevel.Initialize();
            CurrentLevel.LevelReady += OnLevelReady;
            CurrentLevel.LevelCompleted += OnLevelComplete;
            LevelScoreDecided?.Invoke(CurrentLevel.scoreRequirement);
        }

        private void OnLevelReady()
        {
            CurrentLevel.LevelReady -= OnLevelReady;
            StartCoroutine(HyperdriveWaiter());
            HyperDriveMessage?.Invoke("Press Pause Button To Continue...");
        }

        private IEnumerator HyperdriveWaiter()
        {
            while (waitInHyperdrive)
            {
                yield return null;
            }
            NextLevelReady?.Invoke();
        }

        private void OnLevelComplete()
        {
            CurrentLevel.LevelCompleted -= OnLevelComplete;
            waitInHyperdrive = true;
            LevelCompleted?.Invoke();
            
            CurrentLevel.DisposeLevel();
        
            if (IsThereNextLevel())
            {
                var curLevel = _levelsList.FirstOrDefault(x => x.name == CurrentLevel.name);
                OpenLevel();
            }
            else
            {
                GameOver?.Invoke();
            }
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.Pause))
            {
                CloseHyperdrive();
            }
        }

        public void CloseHyperdrive()
        {
            waitInHyperdrive = false;
        }

        private bool IsThereNextLevel()
        {
            return (_levelsList.Count > 0);
        }
    }
}
