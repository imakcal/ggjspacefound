﻿using System;
using System.Collections;
using Game.Scripts.Behaviours;
using Game.Scripts.UI;
using Game.Scripts.UI.Elements;
using UnityEngine;
using UnityEngine.Video;

namespace Game.Scripts.Controllers
{
    public class GameController : MonoBehaviour
    {
        public static event Action<string> DisplayMessage;

        [SerializeField] private AudioClip destroySound;
        [SerializeField] private AudioSource mainAudioSource;
        [SerializeField] private VideoPlayer _videoPlayer;
        [SerializeField] private VideoClip _startingClip;
        [SerializeField] private VideoClip _endingClip;
        public PlayerShipBehaviour playerShip;
        
        private int _videoPlayed;
        
        public LevelController levelController;
        public UIController uIController;
        [HideInInspector] public static GameController instance;
        
        public static event Action<int> ScoreChanged; 
        private int _score;

        private void Awake()
        {
            _videoPlayer.enabled = false;
            AddInstance();
            SetupVideoPlayer();
            levelController.NextLevelReady += OnNextLevelReady;
            levelController.LevelCompleted += OnLevelCompleted;
            PlayButtonElement.StartTheGame += OnGameStarted;
            DebrisBehaviour.UpdateScore += OnScoreUpdated;
            EnemyShipBehaviour.UpdateScore += OnScoreUpdated;
            PlayerShipBehaviour.Destroyed += OnPlayerDied;
            LevelController.GameOver += OnGameOver;
        }

        private void SetupVideoPlayer()
        {
            _videoPlayer.renderMode = VideoRenderMode.CameraNearPlane;
        }

        private void PlayVideo(VideoClip videoClip)
        {
            playerShip.gameObject.SetActive(false);
            _videoPlayer.enabled = true;
            _videoPlayer.clip = videoClip;
            _videoPlayer.Play();
            StartCoroutine(WaitForVideo(videoClip));
        }

        private IEnumerator WaitForVideo(VideoClip videoClip)
        {
            yield return new WaitForSecondsRealtime((float)videoClip.length);
            _videoPlayed++;
            _videoPlayer.enabled = false;
            playerShip.gameObject.SetActive(true);
            if (_videoPlayed == 1)
            {
                OnPlayStarted();
            }
            else
            {
                OpenTheEnd();
            }
        }

        private void OnGameStarted()
        {
            PlayVideo(_startingClip);
        }

        private void OnGameOver()
        {
            PlayVideo(_endingClip);
        }

        private void OnPlayStarted()
        {
            levelController.OnGameStarted();
        }
        
        private void OnLevelCompleted()
        {
            playerShip.transform.parent = transform;
            playerShip.OnLevelEnded();
        }
        
        private void OnNextLevelReady()
        {
            playerShip.transform.parent = levelController.CurrentLevel.transform;
            playerShip.transform.position = levelController.CurrentLevel.SpawnPoint.position;
            playerShip.transform.rotation = levelController.CurrentLevel.SpawnPoint.rotation;
            playerShip.OnLevelStarted();
        }

        private void OpenTheEnd()
        {
            uIController.OpenTheEndScene();
        }

        private void OnScoreUpdated(int scoreChange)
        {
            _score = Mathf.Clamp(_score + scoreChange, 0, 2147483647);
            ScoreChanged?.Invoke(_score);
            CheckIfLevelDone();
        }

        private void CheckIfLevelDone()
        {
            if (_score >= levelController.CurrentLevel.scoreRequirement)
            {
                levelController.CurrentLevel.OnMissionsCompleted();
            }
        }

        private void OnPlayerDied(string message)
        {
            DisplayMessage?.Invoke(message);
            PlaySoundInGameCam(destroySound);
        }

        private void PlaySoundInGameCam(AudioClip sound)
        {
            mainAudioSource.PlayOneShot(sound);
        }

        private void AddInstance()
        {
            if (!instance)
            {
                instance = this;
            }
        }
    }
}
