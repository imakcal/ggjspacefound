﻿using System;
using Game.Scripts.Behaviours;
using Game.Scripts.Controllers;
using Game.Scripts.UI.Elements;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private MessageElement messageElement;
        [SerializeField] private HealthBarElement healthBarElement;
        [SerializeField] private ScoreElement scoreDisplayElement;
        [SerializeField] private RequiredScoreElement requiredScoreElement;
        [SerializeField] private GameObject endScene;

        private bool endSceneActive;
        private void Awake()
        {
            PlayerShipBehaviour.HealthChanged += OnPlayerShipHealthChange;
            EnemyShipBehaviour.PlayerDetected += DisplayMessage;
            GameController.ScoreChanged += OnScoreChange;
            GameController.DisplayMessage += DisplayMessage;
            LevelBehaviour.PlayerLeavingArea += DisplayMessage;
            LevelBehaviour.PortalSpawned += DisplayMessage;
            LevelController.LevelScoreDecided += SetRequiredScore;
            LevelController.HyperDriveMessage += DisplayMessage;
        }

        private void DisplayMessage(string message)
        {
            messageElement.Diplayer(message);
        }
        
        public void OpenTheEndScene()
        {
            endScene.SetActive(true);
            endSceneActive = true;
        }

        private void Update()
        {
            if (!endSceneActive) return;
            if (Input.anyKey)
            {
                Application.Quit();
            }
        }

        private void ChangeHealth(int healthAmount, int maxHealthAmount)
        {
            healthBarElement.SetHealth(healthAmount, maxHealthAmount);
        }

        private void OnPlayerShipHealthChange(int healthAmount, int maxHealthAmount)
        {
            ChangeHealth(healthAmount, maxHealthAmount);
        }

        private void OnScoreChange(int totalScore)
        {
            scoreDisplayElement.UpdateScore(totalScore.ToString());
        }

        private void SetRequiredScore(int newScore)
        {
            requiredScoreElement.UpdateScoreText(newScore.ToString());
        }
    }
}
