﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace Game.Scripts.UI.Elements
{
    public class MessageElement : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI messageText;
        [SerializeField] private GameObject baseElement;
        private Coroutine _messageTextCoroutine;

        public void Diplayer(string message)
        {
            if (_messageTextCoroutine != null) StopCoroutine(_messageTextCoroutine);
            _messageTextCoroutine = StartCoroutine(MessageDisplayerCoroutine());
            messageText.text = message;
        }

        private IEnumerator MessageDisplayerCoroutine()
        {
            baseElement.SetActive(true);
            yield return new WaitForSeconds(2f);
            baseElement.SetActive(false);
            StopCoroutine(_messageTextCoroutine);
        }
    }
}