﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI.Elements
{
    public class PlayButtonElement : MonoBehaviour
    {
        public static event Action StartTheGame;
        [SerializeField] private CanvasGroup arrow;
        [SerializeField] private GameObject page;

        private void Awake()
        {
            arrow.DOFade(0.3f, 2f).SetLoops(-1, LoopType.Yoyo);
            GetComponent<Button>().onClick.AddListener(StartGame);
        }

        private void StartGame()
        {
            StartTheGame?.Invoke();
            Destroy(page);
        }
    }
}
