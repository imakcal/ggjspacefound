﻿using TMPro;
using UnityEngine;

namespace Game.Scripts.UI.Elements
{
    public class ScoreElement : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        
        public void UpdateScore(string score)
        {
            scoreText.text = score;
        }
    }
}
