﻿using TMPro;
using UnityEngine;

namespace Game.Scripts.UI.Elements
{
    public class RequiredScoreElement : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;

        public void UpdateScoreText(string score)
        {
            scoreText.text = score;
        }
    }
}
