﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI.Elements
{
    public class HealthBarElement : MonoBehaviour
    {
        public Image filler;
        public TextMeshProUGUI healthText;

        public void SetHealth(int health, int maxHealth)
        {
            SetHealthFill(health, maxHealth);
            SetHealthText($"{health}/{maxHealth}");
        }

        private void SetHealthText(string health)
        {
            healthText.text = health;
        }

        private void SetHealthFill(int healthAmount, int maxHealthAmount)
        {
            filler.fillAmount = (float)healthAmount / maxHealthAmount;
        }
    }
}